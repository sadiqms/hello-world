#!/bin/sh

docker pull sadiqms/task:image_latest

sleep 10

docker stop $(docker ps -a -q)

docker run -d -p 8000:8000 sadiqms/task:image_latest
exit 0